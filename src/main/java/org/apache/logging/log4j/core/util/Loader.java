package org.apache.logging.log4j.core.util;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache license, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */

import org.apache.logging.log4j.util.LoaderUtil;

import java.lang.reflect.ReflectPermission;


/**
 * Load resources (or images) from various sources.
 */
public final class Loader
{
    static
    {
        final SecurityManager sm = System.getSecurityManager();
        if (sm != null)
        {
            sm.checkPermission(new RuntimePermission("getStackTrace"));
            sm.checkPermission(new ReflectPermission("suppressAccessChecks"));
        }
    }


    /**
     * Load a Class by name. Note that unlike {@link ClassLoader#loadClass(String) ClassLoader.loadClass}, this method
     * will initialize the class as well if it hasn't been already. This is equivalent to the calling the {@link
     * ClassLoader#loadClass(String, boolean) protected version} with the second parameter equal to {@code true}.
     *
     * @param className The class name.
     * @return The Class.
     * @throws ClassNotFoundException if the Class could not be found.
     */
    public static Class<?> loadClass(final String className) throws ClassNotFoundException
    {
        return LoaderUtil.loadClass(className);
    }

    /**
     * Loads and initializes a named Class using a given ClassLoader.
     *
     * @param className The class name.
     * @param loader The class loader.
     * @return The class.
     * @throws ClassNotFoundException if the class could not be found.
     */
    public static Class<?> loadClass(final String className, final ClassLoader loader)
            throws ClassNotFoundException
    {
        return Class.forName(className, true, loader);
    }

    /**
     * Load a Class by name uninitialized.
     *
     * @param className The class name.
     * @return The Class.
     * @throws ClassNotFoundException if the Class could not be found.
     */
    public static Class<?> loadUninitializedClass(final String className) throws ClassNotFoundException
    {
        return LoaderUtil.loadClass(className, false);
    }

    /**
     * Loads but does NOT initializes a named Class using a given ClassLoader.
     *
     * @param className The class name.
     * @param loader The class loader.
     * @return The class.
     * @throws ClassNotFoundException if the class could not be found.
     */
    public static Class<?> loadUninitializedClass(final String className, final ClassLoader loader) throws ClassNotFoundException
    {
        return Class.forName(className, false, loader);
    }

    private Loader()
    {
    }
}
