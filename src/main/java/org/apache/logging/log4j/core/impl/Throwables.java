package org.apache.logging.log4j.core.impl;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache license, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */

import org.apache.logging.log4j.status.StatusLogger;

import java.lang.reflect.Method;


/**
 * Helps with Throwable objects.
 */
public final class Throwables
{

    private static final Method ADD_SUPPRESSED;

    private static final Method GET_SUPPRESSED;

    static
    {
        Method getSuppressed = null, addSuppressed = null;
        final Method[] methods = Throwable.class.getMethods();
        for (final Method method : methods)
        {
            if (method.getName().equals("getSuppressed"))
            {
                getSuppressed = method;
            }
            else if (method.getName().equals("addSuppressed"))
            {
                addSuppressed = method;
            }
        }
        GET_SUPPRESSED = getSuppressed;
        ADD_SUPPRESSED = addSuppressed;
    }

    /**
     * Has no effect on Java 6 and below.
     *
     * @param throwable a Throwable
     * @return see Java 7's {@link Throwable# getSuppressed()}
     * @see Throwable# getSuppressed()
     * @ deprecated If compiling on Java 7 and above use {@link Throwable# getSuppressed()}. Marked as deprecated because
     * Java 6 is deprecated.
     */
    //@Deprecated
    public static Throwable[] getSuppressed(final Throwable throwable)
    {
        if (GET_SUPPRESSED != null)
        {
            try
            {
                return (Throwable[]) GET_SUPPRESSED.invoke(throwable);
            }
            catch (final Exception e)
            {
                // Only happens on Java >= 7 if this class has a bug.
                StatusLogger.getLogger().error(e);
                return null;
            }
        }
        return null;
    }

    private Throwables()
    {
    }

}
