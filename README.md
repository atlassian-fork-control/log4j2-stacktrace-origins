  This bundle of code is taken from the log4j2 code base.  Its mostly copied as is but with
  un-used code removed to make it smaller.
 
  The main class files have been left in place however.  The ThrowableProxy is the one
  we are after and the other classes are support code for it.
 
  This gives us the ability to see "packaging information" in stack traces which will help
  us do a better job in support.
 
  This code will no longer be needed when our code bases (such as JIRA and Confluence) use
  log4j2 or even log-back.  But until then this code gives us better support at the expense of copying
  it.
 
  A cut down version was created so that we don't offer log4j-2 into our code ecosphere and hence have 500
  developers thinking they can use log4j-2 in JIRA say and the number of suggested loggers in IDE's and so forth stays
  the same as today.
 
 
  Since this is Apache code it is copyright them and is covered by their ASF licence

  http://www.apache.org/licenses/LICENSE-2.0
 
